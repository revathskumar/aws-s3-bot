'use strict'

const Telegram = require('telegram-node-bot')
const S3FS = require('s3fs');
const fs = require('fs');
const request = require('request');
require('dotenv').config();

const TELEGRAM_BOT_TOKEN = process.env.TELEGRAM_BOT_TOKEN;

const TelegramBaseController = Telegram.TelegramBaseController
const TextCommand = Telegram.TextCommand
const CustomFilterCommand = require('telegram-node-bot/lib/routing/commands/CustomFilterCommand');
const tg = new Telegram.Telegram(TELEGRAM_BOT_TOKEN, {
    workers: 1,
    webhook: {
        url: process.env.WEBHOOK_URL,
        port: process.env.PORT || 3000,
        host: process.env.WEBHOOK_HOST || 'localhost'
    }
})

var bucketPath = 's3-bot';
var s3Options = {
  region: 'ap-south-1',
  accessKeyId:  process.env.AWS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
};
var s3fs = new S3FS(bucketPath, s3Options);

class PingController extends TelegramBaseController {
    /**
     * @param {Scope} $
     */
    pingHandler($) {
        $.sendMessage('pong')
    }

    get routes() {
        return {
            'pingCommand': 'pingHandler'
        }
    }
}

class OtherwiseController extends TelegramBaseController {
  handle($) {
    $.sendMessage('Sorry, I couldn\'t understand you');
  }
}

class UploadHandlerController extends TelegramBaseController {
    handle($) {
        // console.log('otherwise');
        console.log($.message);
        if ($.message.from.username !== 'revathskumar') {
          $.sendMessage('This is now in private beta. Please wait for the public announcement. Thanks')
          return;
        }
        if ($.message.photo) {
            const telgramFile = $.api.getFile($.message.photo[$.message.photo.length - 1].fileId);
            telgramFile.then(res => {
                const filePathComp = res.filePath.split('/');
                const filename = filePathComp[filePathComp.length - 1];
                const req = request({
                    method: 'GET',
                    uri: `https://api.telegram.org/file/bot${TELEGRAM_BOT_TOKEN}/${res.filePath}`,
                })
                
                req.pipe(s3fs.createWriteStream(filename))
                req.on('end', ()=> {
                    console.log('file wrote');
                })
            });
            
            
        }

        if ($.message.document) {
            const telgramFile = $.api.getFile($.message.document.fileId);
            telgramFile.then(res => {
                const filePathComp = res.filePath.split('/');
                const filename = filePathComp[filePathComp.length - 1];
                const req = request({
                    method: 'GET',
                    uri: `https://api.telegram.org/file/bot${TELEGRAM_BOT_TOKEN}/${res.filePath}`,
                })
                req.pipe(s3fs.createWriteStream(filename))
                req.on('end', ()=> {
                    console.log('file wrote');
                })
            });
        }
    }
}

tg.router
    .when(
        new TextCommand('ping', 'pingCommand'),
        new PingController()
    )
    .when(
        new CustomFilterCommand($ => {
            return Boolean($.message.photo) ||Boolean($.message.document)
        }),
        new UploadHandlerController()
    )
    .otherwise(new OtherwiseController())